package org.aarna.modules;

import groovy.json.JsonSlurper
import org.camunda.bpm.engine.runtime.Execution

/**
 * Utility class to parse the response
 */
class JsonParser {
    private Object   result;
	/*Used by Camunda*/
    JsonParser(Execution execution){ 
		println("In JsonParser Execution....");
        /* fetch execution variable called jsonString */
        def inputString = execution.getVariable("responseOut");
        this.result 	= new JsonSlurper().parseText(inputString.toString());
    }
	/*Used By JUnit*/
	JsonParser(String jsonStr){
		this.result = new JsonSlurper().parseText(jsonStr);
	}
    public String getStatus(){
        return this.result.status;
    }
    public int getStatusCode(){
        return result.status.code;
    }
    public boolean setStatusFlag(Execution execution){
        boolean processStatusFlag = false;
        if(this.result.status.code == 200)
            processStatusFlag=true;
        println "processStatusFlag:";println processStatusFlag;
		if(execution != null) {
            execution.setVariable("scriptOut", processStatusFlag);
            execution.setVariable("processStatus", processStatusFlag);
        }
		return processStatusFlag;
    }
}
