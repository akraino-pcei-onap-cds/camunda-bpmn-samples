package org.aarna.modules;

import org.camunda.bpm.engine.runtime.Execution

/**
 * Utility class to prepare the payload
 */
class HttpsRequest{
    private static final String payload = '''
{
        "commonHeader": {
                "originatorId": "System",
                "requestId": "123456",
                "subRequestId": "1234-12234"
        },
        "actionIdentifiers": {
                "blueprintName": "ansible-playbook-executor",
                "blueprintVersion": "1.0.0",
                "actionName": "execute-ansible-playbook",
                "mode": "sync"
        },
        "payload": {
                "execute-ansible-playbook-request": {
                        "implementation": {
                                "timeout": 3000
                        },
                        "cds-py-exec-pod-ip": "cds-py-executor",                       
                        "py-exec-grcp-timeout": "30000",
                        "workflow-name": "execute-ansible-playbook",
                        "skip-input-params-keys-validation": ["ansible-cli-env-variables"],
                        "input-params":{
                                "ssh-key-git-repository": {
                                        "git-user": "USER_INPUT_GIT_USER",
                                        "git-access-token": "USER_INPUT_GIT_ACCESS_TOKEN",
                                        "git-url": "USER_INPUT_GIT_URL",
                                        "git-branch": "USER_INPUT_GIT_BRANCH",
                                        "git-download-folder": "/opt/app/onap/blueprints",
                                        "git-project-folder": "USER_INPUT_GIT_PROJECT_FOLDER",
                                        "ssh-key-file-name": "USER_INPUT_SSH_KEY_FILE"
                                },
                                "ansible-scripts-git-repository": {
                                        "git-user": "USER_INPUT_GIT_USER",
                                        "git-access-token": "USER_INPUT_GIT_ACCESS_TOKEN",
                                        "git-url": "USER_INPUT_GIT_URL",
                                        "git-branch": "USER_INPUT_GIT_BRANCH",
                                        "git-download-folder": "/opt/app/onap/blueprints",
                                        "git-project-folder": "USER_INPUT_GIT_PROJECT_FOLDER",
                                        "ansible-scripts-folder": "USER_INPUT_ANSIBLE_SCRIPT_FOLDER",
                                        "ansible-main-yaml-file-name": "USER_INPUT_ANSIBLE_MAIN_YAML_FILE"
                                },
                                "ansible-cli-env-variables": {
                                        "target_ip": "USER_INPUT_TARPET_IP",
                                        "target_host_user": "USER_INPUT_TARGET_HOST_USER",
                                        "ansible_ssh_user": "USER_INPUT_ANSIBLE_SSH_USER"
                                }
                        }
                }
        }
}
'''

    HttpsRequest(Execution execution){}
    HttpsRequest(){}
    /**
     * This creates the JSON Payload which will be sent to CDS.
     * 		Replaces pnfIpv4Address, streamCount and resolutionKey which is part of northBound payload
     * */
    public void createPayload(Execution execution) {
        println("Starting with sendPostRequest with URL:");
        execution.setVariable("cdsPayload", payload); /*Used by Camunda to send the HTTP POST Request.*/
        println("Done with createPayload...");
    }
}

