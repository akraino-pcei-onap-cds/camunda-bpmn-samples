# bpmn-ansible-cba-sample
This repository contains PCEI Camunda workflow sample required for the Akrino demo.

# Camunda BPM Process Application
A Process Application for [Camunda BPM](http://docs.camunda.org).

## Prerequisite
Following to be installed to run this application

1. Install Maven 3.6.3 (https://maven.apache.org/docs/3.6.3/release-notes.html)
2. Install JRE 11
3. Install Camunda Modeller (https://camunda.com/download/modeler/)

## Show me the important parts!
- DMN Process
   -  ![](images/COMPONENTS_DMN.png)
- BPMN Process
   -  ![](images/BPMN_SAMPLE_PROCESS_MAIN.png)


### Camunda Modeller
   1. Open Camunda Modeller   
   2. Load DMN in Modeller

      In Modeller , click File -> Open File -> Select COMPONENTS_DMN.dmn file
      ![](images/MODELER_COMPONENTS_DMN.png)

   3. Load BPMN in Modeller

      In Modeller , click File -> Open File -> Select BPMN_SAMPLE_PROCESS_MAIN.bpmn 
      ![](images/MODELER_SAMPLE_BPMN.png)

### Deployment to Camunda Pod
Build and deploy the process application to camunda pod.


#### Manually
1. Update the BPMN process payload 

##### Sample request payload and update the  USER_INPUT_XX values in the [BPMN Process payload](src/main/groovy/org/aarna/modules/HttpsRequest.groovy) file
        
    # USER_INPUT_GIT_USER - git login user name
    # USER_INPUT_GIT_ACCESS_TOKEN - generate the git access token and paste it here.
    # USER_INPUT_GIT_URL - git url
    # USER_INPUT_GIT_PROJECT_FOLDER  - Git  project id. ex. 26901776
    # USER_INPUT_GIT_BRANCH - git branch name ex. "development"
    # USER_INPUT_GIT_DOWNLOAD_FOLDER - Always give "/opt/app/onap/blueprints/deploy"
    # USER_INPUT_SSH_KEY_FILE - ssh key
    # USER_INPUT_ANSIBLE_SCRIPT_FOLDER - ansible script folder 
    # USER_INPUT_ANSIBLE_MAIN_YAML_FILE - ansible main file name
    # USER_INPUT_TARPET_IP - Target IP
    # USER_INPUT_TARGET_HOST_USER - Target host user name
    # USER_INPUT_ANSIBLE_SSH_USER - Ansible SSH user name

[BPMN Process payload](src/main/groovy/org/aarna/modules/HttpsRequest.groovy)

      {
        "commonHeader": {
                "originatorId": "System",
                "requestId": "123456",
                "subRequestId": "1234-12234"
        },
        "actionIdentifiers": {
                "blueprintName": "ansible-playbook-executor",
                "blueprintVersion": "1.0.0",
                "actionName": "execute-ansible-playbook",
                "mode": "sync"
        },
        "payload": {
                "execute-ansible-playbook-request": {
                        "implementation": {
                                "timeout": 3000
                        },
                         "cds-py-exec-pod-ip": "cds-py-executor",
                        "py-exec-grcp-timeout": "30000",
                        "workflow-name": "execute-ansible-playbook",
                        "skip-input-params-keys-validation": ["ansible-cli-env-variables"],
                        "input-params": {
                                "ssh-key-git-repository": {
                                        "git-user": "USER_INPUT_GIT_USER",
                                        "git-access-token": "USER_INPUT_GIT_ACCESS_TOKEN",
                                        "git-url": "USER_INPUT_GIT_URL",
                                        "git-branch": "USER_INPUT_GIT_BRANCH",
                                        "git-download-folder": "/opt/app/onap/blueprints",
                                        "git-project-folder": "USER_INPUT_GIT_PROJECT_FOLDER",
                                        "ssh-key-file-name": "USER_INPUT_SSH_KEY_FILE"
                                },
                                "ansible-scripts-git-repository": {
                                        "git-user": "USER_INPUT_GIT_USER",
                                        "git-access-token": "USER_INPUT_GIT_ACCESS_TOKEN",
                                        "git-url": "USER_INPUT_GIT_URL",
                                        "git-branch": "USER_INPUT_GIT_BRANCH",
                                        "git-download-folder": "/opt/app/onap/blueprints",
                                        "git-project-folder": "USER_INPUT_GIT_PROJECT_FOLDER",
                                        "ansible-scripts-folder": "USER_INPUT_ANSIBLE_SCRIPT_FOLDER",
                                        "ansible-main-yaml-file-name": "USER_INPUT_ANSIBLE_MAIN_YAML_FILE"
                                },
                                "ansible-cli-env-variables": {
                                        "target_ip": "USER_INPUT_TARPET_IP",
                                        "target_host_user": "USER_INPUT_TARGET_HOST_USER",
                                        "ansible_ssh_user": "USER_INPUT_ANSIBLE_SSH_USER"
                                }
                        }
                }
           }
      }



2. Build the application using:

```bash
mvn clean install
```

3. Camunda up and running

        $ kubectl get pods -n emco | grep camunda
        
        amcop-camunda-ddb4cc7c-cswsq   1/1     Running    6          7d23h

4. Get the Camunda service IP   

        $ kubectl get svc -n emco | grep camunda
        
        camunda  NodePort    10.244.8.51     <none>        8443:32061/TCP               135d


3. Copy the bpmn-ansible-cba-sample.war file from the `target` directory to the deployment directory
of the Camunda pod e.g. copy bpmn-ansible-cba-sample.war to pod camunda/bpmnapps folder.

        kubectl -n emco cp <WAR_FILE_PATH> amcop-camunda-ddb4cc7c-cswsq:/camunda/bpmnapps/. 

4. Start the BPMN worflow [BPMN SAMPLE PROCESS](src/main/resources/BPMN_SAMPLE_PROCESS_MAIN.bpmn)

        curl -k -v -X POST  -u souser:mypassword https://10.244.8.51:8443/engine-rest/process-definition/key/PROCESS_BPMN_SAMPLE_MAIN/start --header 'Content-Type: application/json' --data-raw  '{"withVariablesInReturn":true,"variables": {"payload": {"type": "String","value": "{\"sampleData\": \"sample\"}" }}}'

   **Success Response Sample**

         {
            "links": [{
               "method": "GET",
               "href": "https://10.244.8.51:8443/engine-rest/process-instance/86ff5bcd-bcb4-11ec-a99e-42b134d1227f",
               "rel": "self"
            }],
            "id": "86ff5bcd-bcb4-11ec-a99e-42b134d1227f",
            "definitionId": "PROCESS_BPMN_SAMPLE_MAIN:1:b61eef00-bcac-11ec-a99e-42b134d1227f",
            "businessKey": null,
            "caseInstanceId": null,
            "ended": true,
            "suspended": false,
            "tenantId": null,
            "variables": {
               "cdsPayload": {
                  "type": "String",
                  "value": "\n{ \"commonHeader\": { \"originatorId\": \"System\", \"requestId\": \"123456\", \"subRequestId\": \"1234-12234\" }, \"actionIdentifiers\": { \"blueprintName\": \"ansible-playbook-executor\", \"blueprintVersion\": \"1.0.0\", \"actionName\": \"execute-ansible-playbook\", \"mode\": \"sync\" }, \"payload\": { \"execute-ansible-playbook-request\": { \"implementation\": { \"timeout\": 3000 }, \"cds-py-exec-pod-ip\": \"cds-py-executor\", \"py-exec-grcp-timeout\": \"30000\", \"workflow-name\": \"execute-ansible-playbook\", \"skip-input-params-keys-validation\": [\"ansible-cli-env-variables\"], \"input-params\": { \"ssh-key-git-repository\": { \"git-user\": \"\", \"git-access-token\": \"\", \"git-url\": \"\", \"git-branch\": \"\", \"git-download-folder\": \"\/opt\/app\/onap\/blueprints\", \"git-project-folder\": \"\", \"ssh-key-file-name\": \"\" }, \"ansible-scripts-git-repository\": { \"git-user\": \"\", \"git-access-token\": \"\", \"git-url\": \"\", \"git-branch\": \"\", \"git-download-folder\": \"\/opt\/app\/onap\/blueprints\", \"git-project-folder\": \"\", \"ansible-scripts-folder\": \"\", \"ansible-main-yaml-file-name\": \"\" }, \"ansible-cli-env-variables\": { \"target_ip\": \"\", \"target_host_user\": \"\", \"ansible_ssh_user\": \"\" } } } } } \n",
                  "valueInfo": {}
               },
               "cdsUrlOutput": {
                  "type": "String",
                  "value": "http://cds-blueprints-processor-http:8080/api/v1/execution-service/process",
                  "valueInfo": {}
               },
               "processStatus": {
                  "type": "Boolean",
                  "value": true,
                  "valueInfo": {}
               },
               "scriptOut": {
                  "type": "Boolean",
                  "value": true,
                  "valueInfo": {}
               },
               "count": {
                  "type": "String",
                  "value": "0",
                  "valueInfo": {}
               },
               "compSize": {
                  "type": "Integer",
                  "value": 1,
                  "valueInfo": {}
               },
               "dmncbaStatusOut": {
                  "type": "Boolean",
                  "value": true,
                  "valueInfo": {}
               },
               "authorization": {
                  "type": "String",
                  "value": "Basic Y2NzZGthcHBzOmNjc2RrYXBwcw==",
                  "valueInfo": {}
               },
               "password": {
                  "type": "String",
                  "value": "ccsdkapps",
                  "valueInfo": {}
               },
               "responseOut": {
                  "type": "String",
                  "value": "{\"commonHeader\":{\"timestamp\":\"2022-04-15T12:06:48.904Z\",\"originatorId\":\"System\",\"requestId\":\"123456\",\"subRequestId\":\"1234-12234\",\"flags\":null},\"actionIdentifiers\":{\"blueprintName\":\"ansible-playbook-executor\",\"blueprintVersion\":\"1.0.0\",\"actionName\":\"execute-ansible-playbook\",\"mode\":\"sync\"},\"correlationUUID\":null,\"status\":{\"code\":200,\"eventType\":\"EVENT_COMPONENT_EXECUTED\",\"timestamp\":\"2022-04-15T12:06:56.135Z\",\"errorMessage\":null,\"message\":\"success\"},\"payload\":{\"execute-ansible-playbook-response\":{\"status\":\"success\",\"response-data\":{\"ansible-execute-cmd-status\":{\"stderror\":\"\",\"command-string\":\"ansible-playbook , -e \\\"target_ip=\\\" -e \\\"ansible_ssh_user=\\\" -e \\\"target_host_user=\\\" ,\"stdout\":\"\",\"custom-response-attributes\":[],\"response-code\":\"200\",\"exitcode\":\"0\"},\"py-executor-originator-id\":\"cds-controller\",\"key-response-commands\":[],\"download-private-key-rest-api-status\":{\"response-data\":\"\",\"custom-response-attributes\":[],\"rest-api-end-point\":\"\",\"response-code\":\"200\",\"status\":\"success\"},\"py-executor-subrequest-id\":\"85b86a23-9a51-4d05-995e-3d0bd4a664a6\",\"py-executor-request-id\":\"1234-12234\",\"ansible-script-download-rest-api-status\":{\"rest-api-end-point\":\"\",\"status\":\"success\",\"response-code\":\"200\",\"custom-response-attributes\":[],\"response-data\":\"\"}}}}}",
                  "valueInfo": {}
               },
               "authBasic": {
                  "type": "String",
                  "value": "Basic Y2NzZGthcHBzOmNjc2RrYXBwcw==",
                  "valueInfo": {}
               },
               "payload": {
                  "type": "String",
                  "value": "{\"sampleData\": \"sample\"}",
                  "valueInfo": {}
               },
               "port": {
                  "type": "String",
                  "value": "8080",
                  "valueInfo": {}
               },
               "otherhost": {
                  "type": "String",
                  "value": "cds-py-executor",
                  "valueInfo": {}
               },
               "host": {
                  "type": "String",
                  "value": "cds-blueprints-processor-http",
                  "valueInfo": {}
               },
               "compRecords": {
                  "type": "Object",
                  "value": "rO0ABXNyABNqYXZhLnV0aWwuQXJyYXlMaXN0eIHSHZnHYZ0DAAFJAARzaXpleHAAAAABdwQAAAABc3IAEWphdmEudXRpbC5IYXNoTWFwBQfawcMWYNEDAAJGAApsb2FkRmFjdG9ySQAJdGhyZXNob2xkeHA/QAAAAAAADHcIAAAAEAAAAAZ0ABJweS1leGVjLWF1dGgtdG9rZW50ACJCYXNpYyBZMk56Wkd0aGNIQnpPbU5qYzJScllYQndjdz09dAAIcGFzc3dvcmR0AAljY3Nka2FwcHN0AARwb3J0dAAEODA4MHQAEHB5LWV4ZWMtc3ZjLW5hbWV0AA9jZHMtcHktZXhlY3V0b3J0AARob3N0dAAdY2RzLWJsdWVwcmludHMtcHJvY2Vzc29yLWh0dHB0AAR1c2VydAAJY2NzZGthcHBzeHg=",
                  "valueInfo": {
                     "objectTypeName": "java.util.ArrayList",
                     "serializationDataFormat": "application/x-java-serialized-object"
                  }
               },
               "user": {
                  "type": "String",
                  "value": "ccsdkapps",
                  "valueInfo": {}
               }
            }
         }
  


   **Failure Response Sample**   


            {
               "links": [{
                  "method": "GET",
                  "href": "https://10.244.8.51:8443/engine-rest/process-instance/39b22063-bcb5-11ec-a99e-42b134d1227f",
                  "rel": "self"
               }],
               "id": "39b22063-bcb5-11ec-a99e-42b134d1227f",
               "definitionId": "PROCESS_BPMN_SAMPLE_MAIN:1:b61eef00-bcac-11ec-a99e-42b134d1227f",
               "businessKey": null,
               "caseInstanceId": null,
               "ended": true,
               "suspended": false,
               "tenantId": null,
               "variables": {
                  "cdsPayload": {
                     "type": "String",
                     "value": "\n{ \"commonHeader\": { \"originatorId\": \"System\", \"requestId\": \"123456\", \"subRequestId\": \"1234-12234\" }, \"actionIdentifiers\": { \"blueprintName\": \"ansible-playbook-executor\", \"blueprintVersion\": \"1.0.0\", \"actionName\": \"execute-ansible-playbook\", \"mode\": \"sync\" }, \"payload\": { \"execute-ansible-playbook-request\": { \"implementation\": { \"timeout\": 3000 }, \"cds-py-exec-pod-ip\": \"cds-py-executor\", \"py-exec-grcp-timeout\": \"30000\", \"workflow-name\": \"execute-ansible-playbook\", \"skip-input-params-keys-validation\": [\"ansible-cli-env-variables\"], \"input-params\": { \"ssh-key-git-repository\": { \"git-user\": \"\", \"git-access-token\": \"\", \"git-url\": \"\", \"git-branch\": \"\", \"git-download-folder\": \"\/opt\/app\/onap\/blueprints\", \"git-project-folder\": \"\", \"ssh-key-file-name\": \"\" }, \"ansible-scripts-git-repository\": { \"git-user\": \"\", \"git-access-token\": \"\", \"git-url\": \"\", \"git-branch\": \"\", \"git-download-folder\": \"\/opt\/app\/onap\/blueprints\", \"git-project-folder\": \"\", \"ansible-scripts-folder\": \"\", \"ansible-main-yaml-file-name\": \"\" }, \"ansible-cli-env-variables\": { \"target_ip\": \"\", \"target_host_user\": \"\", \"ansible_ssh_user\": \"\" } } } } } \n",
                     "valueInfo": {}
                  },
                  "cdsUrlOutput": {
                     "type": "String",
                     "value": "http://cds-blueprints-processor-http:8080/api/v1/execution-service/process",
                     "valueInfo": {}
                  },
                  "processStatus": {
                     "type": "Boolean",
                     "value": false,
                     "valueInfo": {}
                  },
                  "scriptOut": {
                     "type": "Boolean",
                     "value": false,
                     "valueInfo": {}
                  },
                  "count": {
                     "type": "String",
                     "value": "0",
                     "valueInfo": {}
                  },
                  "compSize": {
                     "type": "Integer",
                     "value": 1,
                     "valueInfo": {}
                  },
                  "dmncbaStatusOut": {
                     "type": "Boolean",
                     "value": true,
                     "valueInfo": {}
                  },
                  "authorization": {
                     "type": "String",
                     "value": "Basic Y2NzZGthcHBzOmNjc2RrYXBwcw==",
                     "valueInfo": {}
                  },
                  "password": {
                     "type": "String",
                     "value": "ccsdkapps",
                     "valueInfo": {}
                  },
                  "responseOut": {
                     "type": "String",
                     "value": "{\"commonHeader\":{\"timestamp\":\"2022-04-15T12:11:50.595Z\",\"originatorId\":\"System\",\"requestId\":\"123456\",\"subRequestId\":\"1234-12234\",\"flags\":null},\"actionIdentifiers\":{\"blueprintName\":\"ansible-playbook-executor\",\"blueprintVersion\":\"1.0.0\",\"actionName\":\"execute-ansible-playbook\",\"mode\":\"sync\"},\"correlationUUID\":null,\"status\":{\"code\":500,\"eventType\":\"EVENT_COMPONENT_FAILURE\",\"timestamp\":\"2022-04-15T12:11:53.205Z\",\"errorMessage\":\"Failed in ComponentRemoteScriptExecutor : Execute ansible is failed, failed to get execution property(node_templates/execute-ansible-executor/attributes/status)\",\"message\":\"failure\"},\"payload\":{\"execute-ansible-playbook-response\":{}}}",
                     "valueInfo": {}
                  },
                  "authBasic": {
                     "type": "String",
                     "value": "Basic Y2NzZGthcHBzOmNjc2RrYXBwcw==",
                     "valueInfo": {}
                  },
                  "payload": {
                     "type": "String",
                     "value": "{\"sampleData\": \"sample\"}",
                     "valueInfo": {}
                  },
                  "port": {
                     "type": "String",
                     "value": "8080",
                     "valueInfo": {}
                  },
                  "otherhost": {
                     "type": "String",
                     "value": "cds-py-executor",
                     "valueInfo": {}
                  },
                  "host": {
                     "type": "String",
                     "value": "cds-blueprints-processor-http",
                     "valueInfo": {}
                  },
                  "compRecords": {
                     "type": "Object",
                     "value": "rO0ABXNyABNqYXZhLnV0aWwuQXJyYXlMaXN0eIHSHZnHYZ0DAAFJAARzaXpleHAAAAABdwQAAAABc3IAEWphdmEudXRpbC5IYXNoTWFwBQfawcMWYNEDAAJGAApsb2FkRmFjdG9ySQAJdGhyZXNob2xkeHA/QAAAAAAADHcIAAAAEAAAAAZ0ABJweS1leGVjLWF1dGgtdG9rZW50ACJCYXNpYyBZMk56Wkd0aGNIQnpPbU5qYzJScllYQndjdz09dAAIcGFzc3dvcmR0AAljY3Nka2FwcHN0AARwb3J0dAAEODA4MHQAEHB5LWV4ZWMtc3ZjLW5hbWV0AA9jZHMtcHktZXhlY3V0b3J0AARob3N0dAAdY2RzLWJsdWVwcmludHMtcHJvY2Vzc29yLWh0dHB0AAR1c2VydAAJY2NzZGthcHBzeHg=",
                     "valueInfo": {
                        "objectTypeName": "java.util.ArrayList",
                        "serializationDataFormat": "application/x-java-serialized-object"
                     }
                  },
                  "user": {
                     "type": "String",
                     "value": "ccsdkapps",
                     "valueInfo": {}
                  }
               }
            }
