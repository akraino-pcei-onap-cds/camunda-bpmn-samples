# etsi-lfedge-hackathon-workflow
This repository contains PCEI Camunda workflow sample required for the Akrino demo.

# Camunda BPM Process Application
A Process Application for [Camunda BPM](http://docs.camunda.org).

## Prerequisite
Following to be installed to run this application

1. Install Maven 3.6.3 (https://maven.apache.org/docs/3.6.3/release-notes.html)
2. Install JRE 11
3. Install Camunda Modeller (https://camunda.com/download/modeler/)

## Show me the important parts!
- DMN Process
   -  ![](images/COMPONENTS_DMN.png)
- BPMN Process
   -  ![](images/BPMN_ETSI_MEC_PROCESS_MAIN.png)


### Camunda Modeller
   1. Open Camunda Modeller   
   2. Load DMN in Modeller

      In Modeller , click File -> Open File -> Select COMPONENTS_DMN.dmn file
      ![](images/MODELER_COMPONENTS_DMN.png)

   3. Load BPMN in Modeller

      In Modeller , click File -> Open File -> Select BPMN_ETSI_MEC_PROCESS_MAIN.bpmn 
      ![](images/BPMN_ETSI_MEC_PROCESS_MAIN.png)

### Deployment to Camunda Pod
Build and deploy the process application to camunda pod.


#### Manually
1. Update the BPMN process payload 

##### Sample request payload and update the  USER_INPUT_XX values in the [BPMN Process payload](src/main/groovy/org/aarna/modules/HttpsRequest.groovy) file
        
    # USER_INPUT - replace this with user data

[BPMN Process payload](src/main/groovy/org/aarna/modules/HttpsRequest.groovy)

      {
         "commonHeader": {
             "timestamp": "2022-03-17T07:33:08.115Z",
             "originatorId": "System",
             "requestId": "05ec54c4-0738-46cc-bbe7-363de2bfaaaa",
             "subRequestId": "05ec54c4-0738-46cc-bbe7-363de2bfaaaa"
         },
         "actionIdentifiers": {
             "blueprintName": "terraform-plan-executor",
             "blueprintVersion": "1.0.0",
             "actionName": "execute-terraform-plan",
             "mode": "sync"
         },
         "payload": {
             "execute-terraform-plan-request": {
                 "implementation": {
                     "timeout": 300000
                 },
                 "cds-py-exec-pod-ip": "py-executor",
                 "py-exec-grpc-timeout": "2700000",
                 "workflow-name": "execute-terraform-plan",
                 "skip-input-params-keys-validation": [
                     "terraform-environment-variables",
                     "terraform-variable-override",
             "git-project-details"
                 ],
                 "input-params": {
                     "customer-uuid": "PCEI-MEC-NE-ACL-TEST-ST-001",
                     "deployment-instance-id": "PCEI-MEC-NE-ACL-TEST-ST-001",
                     "terraform-input-parameters": {
                         "plan-type-metadata": "EQX_NW_EDGE_VNF_ACL",
                         "git-project-details": {
                             "git-url": "USER_INPUT",
                             "git-user": "USER_INPUT",
                             "git-access-token": "USER_INPUT",
                             "git-branch": "main",
                             "git-download-folder": "/opt/app/onap/amcop-lcm-shared-storage",
                             "git-project-folder": "USER_INPUT"
                         },
                         "terraform-plan-folder": "terraform-plans/equinix-saas/create-equinix-network-edge-vnf-access-acl",
                         "terraform-default-state-file-name": "terraform.tfstate",
                         "terraform-var-file-name": "terraform.tfvars",
                         "terraform-state-file-inside-archive": "tfstate",
                         "terraform-environment-variables": {},
                         "terraform-variable-override": {
                             "equinix_client_id": "USER_INPUT",
                             "equinix_client_secret": "USER_INPUT",
                             "equinix_network_acl_template_name": "pcei-test-ssh-acl-001",
                             "equinix_network_acl_template_description": "pcei-test-ssh-acl-001",
                             "inbound_rule_1_subnet": "USER_INPUT/32",
                             "inbound_rule_1_protocol": "TCP",
                             "inbound_rule_1_src_port": "any",
                             "inbound_rule_1_dst_port": 22,
                             "inbound_rule_2_subnet": "USER_INPUT/32",
                             "inbound_rule_2_protocol": "TCP",
                             "inbound_rule_2_src_port": "any",
                             "inbound_rule_2_dst_port": 22
                         },
                         "terraform-action": "apply",
                         "terraform-workspace-name": "EQX_NW_EDGE_VNF_ACL",
                         "terraform-lock-flag": "false",
                         "terraform-retry-count": "2",
                         "terraform-retry-delay": "10"
                     },
                     "terraform-remote-state-parameters": {
                         "git-project-details": {
                             "git-url": "USER_INPUT",
                             "git-user": "USER_INPUT",
                             "git-access-token": "USER_INPUT",
                             "git-branch": "main",
                             "git-download-folder": "/opt/app/onap/amcop-lcm-shared-storage",
                             "git-project-folder": "terraform-state",
                             "git-retry-count" : "2",
                             "git-retry-delay" : "10"
                         }
                     }
                 }
             }
         }
      }



2. Build the application using:

```bash
mvn clean install
```

3. Camunda up and running

        $ kubectl get pods -n emco | grep camunda
        
        amcop-camunda-ddb4cc7c-cswsq   1/1     Running    6          7d23h

4. Get the Camunda service IP   

        $ kubectl get svc -n emco | grep camunda
        
        camunda  NodePort    10.244.8.51     <none>        8443:32061/TCP               135d


3. Copy the etsi-lfedge-hackathon-workflow.war file from the `target` directory to the deployment directory
of the Camunda pod e.g. copy etsi-lfedge-hackathon-workflow.war to pod camunda/bpmnapps folder.

        kubectl -n emco cp <WAR_FILE_PATH> amcop-camunda-ddb4cc7c-cswsq:/camunda/bpmnapps/. 

4. Start the BPMN worflow [BPMN SAMPLE PROCESS](src/main/resources/BPMN_ETSI_MEC_PROCESS_MAIN.bpmn)

        curl -k -v -X POST  -u souser:mypassword https://10.244.8.51:8443/engine-rest/process-definition/key/PROCESS_BPMN_ETSI_MEC_MAIN/start --header 'Content-Type: application/json' --data-raw  '{"withVariablesInReturn":true,"variables": {"payload": {"type": "String","value": "{\"sampleData\": \"sample\"}" }}}'

   **Success Response Sample**

         {
            "links": [{
               "method": "GET",
               "href": "https://10.244.8.51:8443/engine-rest/process-instance/86ff5bcd-bcb4-11ec-a99e-42b134d1227f",
               "rel": "self"
            }],
            "id": "86ff5bcd-bcb4-11ec-a99e-42b134d1227f",
            "definitionId": "PROCESS_BPMN_ETSI_MEC_MAIN:1:b61eef00-bcac-11ec-a99e-42b134d1227f",
            "businessKey": null,
            "caseInstanceId": null,
            "ended": true,
            "suspended": false,
            "tenantId": null,
            "variables": {
               "vnfAclPayload": {
                  "type": "String",
                  "value": "",
                  "valueInfo": {}
               },
               "cdsUrlOutput": {
                  "type": "String",
                  "value": "http://cds-blueprints-processor-http:8080/api/v1/execution-service/process",
                  "valueInfo": {}
               },
               "processStatus": {
                  "type": "Boolean",
                  "value": true,
                  "valueInfo": {}
               },
               "scriptOut": {
                  "type": "Boolean",
                  "value": true,
                  "valueInfo": {}
               },
               "count": {
                  "type": "String",
                  "value": "0",
                  "valueInfo": {}
               },
               "compSize": {
                  "type": "Integer",
                  "value": 1,
                  "valueInfo": {}
               },
               "dmncbaStatusOut": {
                  "type": "Boolean",
                  "value": true,
                  "valueInfo": {}
               },
               "authorization": {
                  "type": "String",
                  "value": "Basic Y2NzZGthcHBzOmNjc2RrYXBwcw==",
                  "valueInfo": {}
               },
               "password": {
                  "type": "String",
                  "value": "ccsdkapps",
                  "valueInfo": {}
               },
               "responseOut": {
                  "type": "String",
                  "value": "",
                  "valueInfo": {}
               },
               "authBasic": {
                  "type": "String",
                  "value": "Basic Y2NzZGthcHBzOmNjc2RrYXBwcw==",
                  "valueInfo": {}
               },
               "payload": {
                  "type": "String",
                  "value": "{\"sampleData\": \"sample\"}",
                  "valueInfo": {}
               },
               "port": {
                  "type": "String",
                  "value": "8080",
                  "valueInfo": {}
               },
               "otherhost": {
                  "type": "String",
                  "value": "cds-py-executor",
                  "valueInfo": {}
               },
               "host": {
                  "type": "String",
                  "value": "cds-blueprints-processor-http",
                  "valueInfo": {}
               },
               "compRecords": {
                  "type": "Object",
                  "value": "rO0ABXNyABNqYXZhLnV0aWwuQXJyYXlMaXN0eIHSHZnHYZ0DAAFJAARzaXpleHAAAAABdwQAAAABc3IAEWphdmEudXRpbC5IYXNoTWFwBQfawcMWYNEDAAJGAApsb2FkRmFjdG9ySQAJdGhyZXNob2xkeHA/QAAAAAAADHcIAAAAEAAAAAZ0ABJweS1leGVjLWF1dGgtdG9rZW50ACJCYXNpYyBZMk56Wkd0aGNIQnpPbU5qYzJScllYQndjdz09dAAIcGFzc3dvcmR0AAljY3Nka2FwcHN0AARwb3J0dAAEODA4MHQAEHB5LWV4ZWMtc3ZjLW5hbWV0AA9jZHMtcHktZXhlY3V0b3J0AARob3N0dAAdY2RzLWJsdWVwcmludHMtcHJvY2Vzc29yLWh0dHB0AAR1c2VydAAJY2NzZGthcHBzeHg=",
                  "valueInfo": {
                     "objectTypeName": "java.util.ArrayList",
                     "serializationDataFormat": "application/x-java-serialized-object"
                  }
               },
               "user": {
                  "type": "String",
                  "value": "ccsdkapps",
                  "valueInfo": {}
               }
            }
         }
  


   **Failure Response Sample**   


            {
               "links": [{
                  "method": "GET",
                  "href": "https://10.244.8.51:8443/engine-rest/process-instance/39b22063-bcb5-11ec-a99e-42b134d1227f",
                  "rel": "self"
               }],
               "id": "39b22063-bcb5-11ec-a99e-42b134d1227f",
               "definitionId": "PROCESS_BPMN_SAMPLE_MAIN:1:b61eef00-bcac-11ec-a99e-42b134d1227f",
               "businessKey": null,
               "caseInstanceId": null,
               "ended": true,
               "suspended": false,
               "tenantId": null,
               "variables": {
                  "cdsPayload": {
                     "type": "String",
                     "value": "",
                     "valueInfo": {}
                  },
                  "cdsUrlOutput": {
                     "type": "String",
                     "value": "http://cds-blueprints-processor-http:8080/api/v1/execution-service/process",
                     "valueInfo": {}
                  },
                  "processStatus": {
                     "type": "Boolean",
                     "value": false,
                     "valueInfo": {}
                  },
                  "scriptOut": {
                     "type": "Boolean",
                     "value": false,
                     "valueInfo": {}
                  },
                  "count": {
                     "type": "String",
                     "value": "0",
                     "valueInfo": {}
                  },
                  "compSize": {
                     "type": "Integer",
                     "value": 1,
                     "valueInfo": {}
                  },
                  "dmncbaStatusOut": {
                     "type": "Boolean",
                     "value": true,
                     "valueInfo": {}
                  },
                  "authorization": {
                     "type": "String",
                     "value": "Basic Y2NzZGthcHBzOmNjc2RrYXBwcw==",
                     "valueInfo": {}
                  },
                  "password": {
                     "type": "String",
                     "value": "ccsdkapps",
                     "valueInfo": {}
                  },
                  "responseOut": {
                     "type": "String",
                     "value": "{\"commonHeader\":{\"timestamp\":\"2022-04-15T12:11:50.595Z\",\"originatorId\":\"System\",\"requestId\":\"123456\",\"subRequestId\":\"1234-12234\",\"flags\":null},\"actionIdentifiers\":{\"blueprintName\":\"terraform-plan-executor\",\"blueprintVersion\":\"1.0.0\",\"actionName\":\"execute-terraform-plan\",\"mode\":\"sync\"},\"correlationUUID\":null,\"status\":{\"code\":500,\"eventType\":\"EVENT_COMPONENT_FAILURE\",\"timestamp\":\"2022-04-15T12:11:53.205Z\",\"errorMessage\":\"Failed in ComponentRemoteScriptExecutor : Execute ansible is failed, failed to get execution property(node_templates/terraform-plan-executor/attributes/status)\",\"message\":\"failure\"},\"payload\":{\"execute-terraform-plan-response\":{}}}",
                     "valueInfo": {}
                  },
                  "authBasic": {
                     "type": "String",
                     "value": "Basic Y2NzZGthcHBzOmNjc2RrYXBwcw==",
                     "valueInfo": {}
                  },
                  "payload": {
                     "type": "String",
                     "value": "{\"sampleData\": \"sample\"}",
                     "valueInfo": {}
                  },
                  "port": {
                     "type": "String",
                     "value": "8080",
                     "valueInfo": {}
                  },
                  "otherhost": {
                     "type": "String",
                     "value": "cds-py-executor",
                     "valueInfo": {}
                  },
                  "host": {
                     "type": "String",
                     "value": "cds-blueprints-processor-http",
                     "valueInfo": {}
                  },
                  "compRecords": {
                     "type": "Object",
                     "value": "rO0ABXNyABNqYXZhLnV0aWwuQXJyYXlMaXN0eIHSHZnHYZ0DAAFJAARzaXpleHAAAAABdwQAAAABc3IAEWphdmEudXRpbC5IYXNoTWFwBQfawcMWYNEDAAJGAApsb2FkRmFjdG9ySQAJdGhyZXNob2xkeHA/QAAAAAAADHcIAAAAEAAAAAZ0ABJweS1leGVjLWF1dGgtdG9rZW50ACJCYXNpYyBZMk56Wkd0aGNIQnpPbU5qYzJScllYQndjdz09dAAIcGFzc3dvcmR0AAljY3Nka2FwcHN0AARwb3J0dAAEODA4MHQAEHB5LWV4ZWMtc3ZjLW5hbWV0AA9jZHMtcHktZXhlY3V0b3J0AARob3N0dAAdY2RzLWJsdWVwcmludHMtcHJvY2Vzc29yLWh0dHB0AAR1c2VydAAJY2NzZGthcHBzeHg=",
                     "valueInfo": {
                        "objectTypeName": "java.util.ArrayList",
                        "serializationDataFormat": "application/x-java-serialized-object"
                     }
                  },
                  "user": {
                     "type": "String",
                     "value": "ccsdkapps",
                     "valueInfo": {}
                  }
               }
            }
