package org.aarna.modules;

import org.camunda.bpm.engine.runtime.Execution

/**
 * Utility class to prepare the payload
 */
class HttpsRequest{
    private static final String vnfAclPayload = '''
{
    "commonHeader": {
        "timestamp": "2022-03-17T07:33:08.115Z",
        "originatorId": "System",
        "requestId": "05ec54c4-0738-46cc-bbe7-363de2bfaaaa",
        "subRequestId": "05ec54c4-0738-46cc-bbe7-363de2bfaaaa"
    },
    "actionIdentifiers": {
        "blueprintName": "terraform-plan-executor",
        "blueprintVersion": "1.0.0",
        "actionName": "execute-terraform-plan",
        "mode": "sync"
    },
    "payload": {
        "execute-terraform-plan-request": {
            "implementation": {
                "timeout": 300000
            },
            "cds-py-exec-pod-ip": "py-executor",
            "py-exec-grpc-timeout": "2700000",
            "workflow-name": "execute-terraform-plan",
            "skip-input-params-keys-validation": [
                "terraform-environment-variables",
                "terraform-variable-override",
        "git-project-details"
            ],
            "input-params": {
                "customer-uuid": "PCEI-MEC-NE-ACL-TEST-ST-001",
                "deployment-instance-id": "PCEI-MEC-NE-ACL-TEST-ST-001",
                "terraform-input-parameters": {
                    "plan-type-metadata": "EQX_NW_EDGE_VNF_ACL",
                    "git-project-details": {
                        "git-url": "USER_INPUT",
                        "git-user": "USER_INPUT",
                        "git-access-token": "USER_INPUT",
                        "git-branch": "main",
                        "git-download-folder": "/opt/app/onap/amcop-lcm-shared-storage",
                        "git-project-folder": "USER_INPUT"
                    },
                    "terraform-plan-folder": "terraform-plans/equinix-saas/create-equinix-network-edge-vnf-access-acl",
                    "terraform-default-state-file-name": "terraform.tfstate",
                    "terraform-var-file-name": "terraform.tfvars",
                    "terraform-state-file-inside-archive": "tfstate",
                    "terraform-environment-variables": {},
                    "terraform-variable-override": {
                        "equinix_client_id": "USER_INPUT",
                        "equinix_client_secret": "USER_INPUT",
                        "equinix_network_acl_template_name": "pcei-test-ssh-acl-001",
                        "equinix_network_acl_template_description": "pcei-test-ssh-acl-001",
                        "inbound_rule_1_subnet": "USER_INPUT/32",
                        "inbound_rule_1_protocol": "TCP",
                        "inbound_rule_1_src_port": "any",
                        "inbound_rule_1_dst_port": 22,
                        "inbound_rule_2_subnet": "USER_INPUT/32",
                        "inbound_rule_2_protocol": "TCP",
                        "inbound_rule_2_src_port": "any",
                        "inbound_rule_2_dst_port": 22
                    },
                    "terraform-action": "apply",
                    "terraform-workspace-name": "EQX_NW_EDGE_VNF_ACL",
                    "terraform-lock-flag": "false",
                    "terraform-retry-count": "2",
                    "terraform-retry-delay": "10"
                },
                "terraform-remote-state-parameters": {
                    "git-project-details": {
                        "git-url": "USER_INPUT",
                        "git-user": "USER_INPUT",
                        "git-access-token": "USER_INPUT",
                        "git-branch": "main",
                        "git-download-folder": "/opt/app/onap/amcop-lcm-shared-storage",
                        "git-project-folder": "terraform-state",
                        "git-retry-count" : "2",
                        "git-retry-delay" : "10"
                    }
                }
            }
        }
    }
}
'''

private static final String vnfPayload = '''
{
    "commonHeader": {
        "timestamp": "2022-03-17T07:33:08.115Z",
        "originatorId": "System",
        "requestId": "05ec54c4-0738-46cc-bbe7-363de2bfaaaa",
        "subRequestId": "05ec54c4-0738-46cc-bbe7-363de2bfaaaa"
    },
    "actionIdentifiers": {
        "blueprintName": "terraform-plan-executor",
        "blueprintVersion": "1.0.0",
        "actionName": "execute-terraform-plan",
        "mode": "sync"
    },
    "payload": {
        "execute-terraform-plan-request": {
            "implementation": {
                "timeout": 300000
            },
            "cds-py-exec-pod-ip": "py-executor",
            "py-exec-grpc-timeout": "2700000",
            "workflow-name": "execute-terraform-plan",
            "skip-input-params-keys-validation": [
                "terraform-environment-variables",
                "terraform-variable-override"
            ],
            "input-params": {
                "customer-uuid": "PCEI-MEC-NE-CISCO-CSR-TEST-ST-001",
                "deployment-instance-id": "PCEI-MEC-NE-CISCO-CSR-TEST-ST-001",
                "terraform-input-parameters": {
                    "plan-type-metadata": "EQX_NW_EDGE_VNF_CISCO_CSR",
                    "git-project-details": {
                        "git-url": "USER_INPUT",
                        "git-user": "USER_INPUT",
                        "git-access-token": "USER_INPUT",
                        "git-branch": "main",
                        "git-download-folder": "/opt/app/onap/amcop-lcm-shared-storage",
                        "git-project-folder": "USER_INPUT"
                    },
                    "terraform-plan-folder": "terraform-plans/equinix-saas/create-equinix-network-edge-vnf-managed-subscription",
                    "terraform-default-state-file-name": "terraform.tfstate",
                    "terraform-var-file-name": "terraform.tfvars",
                    "terraform-state-file-inside-archive": "tfstate",
                    "terraform-environment-variables": {},
                    "terraform-variable-override": {
                        "equinix_client_id": "USER_INPUT",
                        "equinix_client_secret": "USER_INPUT",
                        "equinix_network_device_metro_code": "USER_INPUT",
                        "equinix_network_account_number": "USER_INPUT",
                        "equinix_network_device_name": "PCEI-MEC-TEST-CSR-01",
                        "equinix_network_device_throughput": 500,
                        "equinix_network_device_throughput_unit": "Mbps",
                        "equinix_network_device_type_code": "CSR1000V",
                        "equinix_network_device_package_code": "SEC",
                        "equinix_network_device_notifications": [
                            "USER_INPUT"
                        ],
                        "equinix_network_device_hostname": "pcei-mec-test-csr-01",
                        "equinix_network_device_term_length_months": 1,
                        "equinix_network_device_version": "16.12.03",
                        "equinix_network_device_core_count": 2,
                        "equinix_network_device_interface_count": 10,
                        "equinix_network_device_acl_template_id": "USER_INPUT",
                        "equinix_network_device_internet_additional_bandwidth_mbps": 15
                    },
                    "terraform-action": "output",
                    "terraform-workspace-name": "EQX_NW_EDGE_VNF_CISCO_CSR",
                    "terraform-lock-flag": "false",
                    "terraform-retry-count": "2",
                    "terraform-retry-delay": "10"
                },
                "terraform-remote-state-parameters": {
                    "git-project-details": {
                        "git-url": "USER_INPUT",
                        "git-user": "USER_INPUT",
                        "git-access-token": "USER_INPUT",
                        "git-branch": "main",
                        "git-download-folder": "/opt/app/onap/amcop-lcm-shared-storage",
                        "git-project-folder": "terraform-state",
                        "git-retry-count" : "2",
                        "git-retry-delay" : "10"
                    }
                }
            }
        }
    }
}
'''

private static final String vnfSshPayload = '''
{
    "commonHeader": {
        "timestamp": "2022-03-17T07:33:08.115Z",
        "originatorId": "System",
        "requestId": "05ec54c4-0738-46cc-bbe7-363de2bfaaaa",
        "subRequestId": "05ec54c4-0738-46cc-bbe7-363de2bfaaaa"
    },
    "actionIdentifiers": {
        "blueprintName": "terraform-plan-executor",
        "blueprintVersion": "1.0.0",
        "actionName": "execute-terraform-plan",
        "mode": "sync"
    },
    "payload": {
        "execute-terraform-plan-request": {
            "implementation": {
                "timeout": 300000
            },
            "cds-py-exec-pod-ip": "py-executor",
            "py-exec-grpc-timeout": "2700000",
            "workflow-name": "execute-terraform-plan",
            "skip-input-params-keys-validation": [
                "terraform-environment-variables",
                "terraform-variable-override"
            ],
            "input-params": {
                "customer-uuid": "PCEI-MEC-NE-SSH-PASS-TEST-ST-001",
                "deployment-instance-id": "PCEI-MEC-NE-SSH-PASS-TEST-ST-001",
                "terraform-input-parameters": {
                    "plan-type-metadata": "EQX_NW_EDGE_VNF_SSH_PASSWORD",
                    "git-project-details": {
                        "git-url": "USER_INPUT",
                        "git-user": "USER_INPUT",
                        "git-access-token": "USER_INPUT",
                        "git-branch": "main",
                        "git-download-folder": "/opt/app/onap/amcop-lcm-shared-storage",
                        "git-project-folder": "USER_INPUT"
                    },
                    "terraform-plan-folder": "terraform-plans/equinix-saas/create-equinix-network-edge-vnf-ssh-password-auth",
                    "terraform-default-state-file-name": "terraform.tfstate",
                    "terraform-var-file-name": "terraform.tfvars",
                    "terraform-state-file-inside-archive": "tfstate",
                    "terraform-environment-variables": {},
                    "terraform-variable-override": {
                        "equinix_client_id": "USER_INPUT",
                        "equinix_client_secret": "USER_INPUT",
                        "ssh_username": "USER_INPUT",
                        "ssh_password": "USER_INPUT",
                        "vnf_device_ids" : ["USER_INPUT"],
                        "equinix_metro_code" : "USER_INPUT",
                        "equinix_network_account_name" : "USER_INPUT"

                    },
                    "terraform-action": "apply",
                    "terraform-workspace-name": "EQX_NW_EDGE_VNF_SSH_PASSWORD",
                    "terraform-lock-flag": "false",
                    "terraform-retry-count": "2",
                    "terraform-retry-delay": "10"
                },
                "terraform-remote-state-parameters": {
                    "git-project-details": {
                        "git-url": "USER_INPUT",
                        "git-user": "USER_INPUT",
                        "git-access-token": "USER_INPUT",
                        "git-branch": "main",
                        "git-download-folder": "/opt/app/onap/amcop-lcm-shared-storage",
                        "git-project-folder": "terraform-state",
                        "git-retry-count" : "2",
                        "git-retry-delay" : "10"
                    }
                }
            }
        }
    }
}
'''

private static final String azurePayload = '''
{
    "commonHeader": {
        "timestamp": "2022-03-17T07:33:08.115Z",
        "originatorId": "System",
        "requestId": "05ec54c4-0738-46cc-bbe7-363de2bfaaaa",
        "subRequestId": "05ec54c4-0738-46cc-bbe7-363de2bfaaaa"
    },
    "actionIdentifiers": {
        "blueprintName": "terraform-plan-executor",
        "blueprintVersion": "1.0.0",
        "actionName": "execute-terraform-plan",
        "mode": "sync"
    },
    "payload": {
        "execute-terraform-plan-request": {
            "implementation": {
                "timeout": 30000
            },
            "cds-py-exec-pod-ip": "py-executor",
            "py-exec-grpc-timeout": "2700000",
            "workflow-name": "execute-terraform-plan",
            "skip-input-params-keys-validation": [
                "terraform-environment-variables",
                "terraform-variable-override"
            ],
            "input-params": {
                "customer-uuid": "PCEI-MEC-AZEXP-RT-TEST-ST-001",
                "deployment-instance-id": "PCEI-MEC-AZEXP-RT-TEST-ST-001",
                "terraform-input-parameters": {
                    "plan-type-metadata": "AZ_EXP_ROUTE_NEW_RG",
                    "git-project-details": {
                         "git-url": "USER_INPUT",
                        "git-user": "USER_INPUT",
                        "git-access-token": "USER_INPUT",
                        "git-branch": "main",
                        "git-download-folder": "/opt/app/onap/amcop-lcm-shared-storage",
                        "git-project-folder": "USER_INPUT"
                    },
                    "terraform-plan-folder": "terraform-plans/equinix-saas/create-azure-l2-exp-route-peering-new-rg",
                    "terraform-default-state-file-name": "terraform.tfstate",
                    "terraform-var-file-name": "terraform.tfvars",
                    "terraform-state-file-inside-archive": "tfstate",
                    "terraform-environment-variables": {
                        "ARM_SUBSCRIPTION_ID": "USER_INPUT",
                        "ARM_CLIENT_ID": "USER_INPUT",
                        "ARM_CLIENT_SECRET": "USER_INPUT",
                        "ARM_TENANT_ID": "USER_INPUT"                        
                    },
                    "terraform-variable-override": {
                        "azurerm_region": "USER_INPUT",
                        "azurerm_peering_location": "USER_INPUT",
                        "azurerm_resource_group_name": "PCEI-MEC-RG-001",
                        "azurerm_express_route_microsoft_peering_config_ipv4_prefixes": ["USER_INPUT"],
                        "azurerm_secondary_peer_address_prefix": "USER_INPUT/30",
                        "azurerm_primary_peer_address_prefix": "USER_INPUT/30",
                        "azurerm_express_route_circuit_name": "USER_INPUT",
                        "azurerm_express_route_peer_asn": USER_INPUT,
                        "azurerm_express_route_peering_type": "AzurePrivatePeering",
                        "azurerm_express_route_circuit_bandwidth": USER_INPUT,
                        "azurerm_express_route_circuit_peering_vlan_id": USER_INPUT,
                        "azurerm_express_route_circuit_peering_shared_key": "USER_INPUT",
                        "azurerm_express_route_service_provider_name": "USER_INPUT",
                        "azurerm_express_route_sku_tier": "Premium",
                        "azurerm_express_route_sku_family": "UnlimitedData",
                        "azurerm_express_route_allow_classic_operations": false,
                        "azurerm_express_route_create_duration_seconds": "60s",
                        "azurerm_express_route_circuit_authorization_name": "USER_INPUT"
                    },
                    "terraform-action": "apply",
                    "terraform-workspace-name": "AZ_EXP_ROUTE_NEW_RG",
                    "terraform-lock-flag": "false"
                },
                "terraform-remote-state-parameters": {
                    "git-project-details": {
                        "git-url": "USER_INPUT",
                        "git-user": "USER_INPUT",
                        "git-access-token": "USER_INPUT",
                        "git-branch": "main",
                        "git-download-folder": "/opt/app/onap/amcop-lcm-shared-storage",
                        "git-project-folder": "terraform-state",
                        "git-retry-count" : "2",
                        "git-retry-delay" : "10"
                    }
                }
            }
        }
    }
}
'''

private static final String azureVnfConnPayload = '''
{
    "commonHeader": {
        "timestamp": "2022-03-17T07:33:08.115Z",
        "originatorId": "System",
        "requestId": "05ec54c4-0738-46cc-bbe7-363de2bfaaaa",
        "subRequestId": "05ec54c4-0738-46cc-bbe7-363de2bfaaaa"
    },
    "actionIdentifiers": {
        "blueprintName": "terraform-plan-executor",
        "blueprintVersion": "1.0.0",
        "actionName": "execute-terraform-plan",
        "mode": "sync"
    },
    "payload": {
        "execute-terraform-plan-request": {
            "implementation": {
                "timeout": 300000
            },
            "cds-py-exec-pod-ip": "py-executor",
            "py-exec-grpc-timeout": "2700000",
            "workflow-name": "execute-terraform-plan",
            "skip-input-params-keys-validation": [
                "terraform-environment-variables",
                "terraform-variable-override"
            ],
            "input-params": {
                "customer-uuid": "PCEI-MEC-NE-L2CONN-TEST-ST-001",
                "deployment-instance-id": "PCEI-MEC-NE-L2CONN-TEST-ST-001",
                "terraform-input-parameters": {
                    "plan-type-metadata": "PCEI-MEC-NE-2CONN",
                    "git-project-details": {
                        "git-url": "USER_INPUT",
                        "git-user": "USER_INPUT",
                        "git-access-token": "USER_INPUT",
                        "git-branch": "main",
                        "git-download-folder": "/opt/app/onap/amcop-lcm-shared-storage",
                        "git-project-folder": "USER_INPUT"
                    },
                    "terraform-plan-folder": "terraform-plans/equinix-saas/create-equinix-network-edge-vnf-az-ha-connectivity",
                    "terraform-default-state-file-name": "terraform.tfstate",
                    "terraform-var-file-name": "terraform.tfvars",
                    "terraform-state-file-inside-archive": "tfstate",
                    "terraform-environment-variables": {},
                    "terraform-variable-override": {
                        "equinix_client_id": "USER_INPUT",
                        "equinix_client_secret": "USER_INPUT",
                        "equinix_ecx_l2_connection_speed" : 50,
                        "equinix_ecx_l2_connection_speed_unit" :"MB",
                        "equinix_azure_metro_code" : "USER_INPUT",
                        "azure_exp_route_service_key" : "USER_INPUT",
                        "equinix_network_account_name" : "USER_INPUT",
                        "equinix_ecx_l2_connection_primary_name": "PCEI-MEC-NE-PRICONN-INT5",
                        "equinix_network_edge_primary_vnf_uuid" : "USER_INPUT",
                        "equinix_network_edge_primary_vnf_interface_number" : USER_INPUT,
                        "equinix_ecx_l2_connection_notifications" : ["USER_INPUT"],
                        "equinix_ecx_l2_connection_notifications_named_tag" : "USER_INPUT",
                        "equinix_network_edge_secondary_vnf_uuid" : "USER_INPUT",
                        "equinix_network_edge_secondary_vnf_interface_number" : USER_INPUT,
                        "equinix_ecx_l2_connection_secondary_name" : "PCEI-MEC-NE-SECCONN-INT6"
                    },
                    "terraform-action": "apply",
                    "terraform-workspace-name": "PCEI-MEC-NE-2CONN",
                    "terraform-lock-flag": "false",
                    "terraform-retry-count": "2",
                    "terraform-retry-delay": "10"
                },
                "terraform-remote-state-parameters": {
                    "git-project-details": {
                        "git-url": "USER_INPUT",
                        "git-user": "USER_INPUT",
                        "git-access-token": "USER_INPUT",
                        "git-branch": "main",
                        "git-download-folder": "/opt/app/onap/amcop-lcm-shared-storage",
                        "git-project-folder": "terraform-state",
                        "git-retry-count" : "2",
                        "git-retry-delay" : "10"
                    }
                }
            }
        }
    }
}
'''

    HttpsRequest(Execution execution){}
    HttpsRequest(){}
    /**
     * This creates the JSON Payload which will be sent to CDS.
     * 		Replaces pnfIpv4Address, streamCount and resolutionKey which is part of northBound payload
     * */
    public void createPayload(Execution execution) {
        println("Starting with sendPostRequest with URL:");
        execution.setVariable("vnfAclPayload", vnfAclPayload); /*Used by Camunda to send the HTTP POST Request.*/
        execution.setVariable("vnfPayload", vnfPayload);
        execution.setVariable("vnfSshPayload", vnfSshPayload);
        execution.setVariable("azurePayload", azurePayload);
        execution.setVariable("azureVnfConnPayload", azureVnfConnPayload);
        println("Done with createPayload...");
    }
}

