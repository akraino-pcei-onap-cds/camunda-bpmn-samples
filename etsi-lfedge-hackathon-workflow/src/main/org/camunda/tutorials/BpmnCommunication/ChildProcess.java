package org.camunda.tutorials.BpmnCommunication;

import java.util.logging.Logger;

import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class ChildProcess implements JavaDelegate {
  private final Logger LOGGER = Logger.getLogger(ChildProcess.class.getName());
  public void execute(DelegateExecution execution) throws InterruptedException {
	LOGGER.info("ChildProcess, Entering and Id is:"+execution.getBpmnModelElementInstance().getId());																																																																				
    RuntimeService runtimeService = execution.getProcessEngineServices().getRuntimeService();
    Map<String, Object> ipVars    = runtimeService.getVariables(execution.getId());
    String messageName			  = (String)ipVars.get("messageName");	
    
    LOGGER.info(messageName + "-ChildProcess, Received Message from Main with vars:"+ipVars);
    String signalName = "Process-Complete"; //By Default, success signal
    //String signalName = "Process-Failed";
    int execCount     = (Integer)ipVars.get("execCount");
    
    Map<String, Object> varMap = new HashMap<>();
    varMap.put("processName", messageName);
    varMap.put("executionId", execution.getId());
    varMap.put("signalName" , signalName);
    varMap.put("execCount"  , execCount );
    TimeUnit.SECONDS.sleep(5);
    LOGGER.info(messageName + "-ChildProcess, Complete...");
  }
}

