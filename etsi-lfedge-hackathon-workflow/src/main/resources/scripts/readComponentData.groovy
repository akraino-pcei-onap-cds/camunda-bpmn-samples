import groovy.json.JsonSlurper

// fetch execution variable called jsonString
Integer compSize = execution.getVariable("compSize");

println "compSize"
println compSize;

temprowData = execution.getVariable("compRecords");
println "compRecords";
println temprowData;

Map<String,String> mapData = new HashMap<String,String>();

if(compSize == 1)
    mapData =  temprowData.get(0);

println "mapData";
println mapData;

println "Map Iterate";
mapData.each{entry -> println "$entry.key: $entry.value"}
String tempHost, tempport, tempUser, tempPass, tempAuth,tempOtherhost;

for (entry in mapData) {
    println "Key: $entry.key = Value: $entry.value"
    if(entry.key == "host")
        tempHost = entry.value;
    if(entry.key == "port")
        tempport = entry.value;
    if(entry.key == "user")
        tempUser = entry.value;
    if(entry.key == "password")
        tempPass = entry.value;
    if(entry.key == "py-exec-auth-token")
        tempAuth = entry.value;
    if(entry.key == "py-exec-svc-name")
        tempOtherhost = entry.value;
}
String tempUrl = "http://"+tempHost+":"+tempport+"/api/v1/execution-service" + "/process";
println tempUrl;
println "Data Extract complete";
execution.setVariable("host", tempHost);
execution.setVariable("port", tempport);
execution.setVariable("user", tempUser);
execution.setVariable("password", tempPass);
execution.setVariable("authorization", tempAuth);
execution.setVariable("cdsUrlOutput", tempUrl);
execution.setVariable("authBasic", tempAuth);
execution.setVariable("otherhost", tempOtherhost);
execution.setVariable("processStatus", false);
execution.setVariable("dmncbaStatusOut", true);

