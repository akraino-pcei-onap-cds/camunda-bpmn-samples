package org.aarna.test;

import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Ignore
import org.junit.Test
import org.aarna.modules.HttpsRequest
public class HttpsRequestTest {
	private static final String getUrl  = "<>";
	private static final String postUrl = "<>";
	//private static final String url = "https://google.com";
	private static HttpsRequest req;
	@BeforeClass
	public static void init() {
		req = new HttpsRequest(postUrl);
	}
	@Before
	public void beforeEachTest() {
		println("Starting With Request test case...");
	}
	@After
	public void afterEachTest() {
		println("Done with Request test case...");
	}
	@Ignore
	@Test
	public void getRequest() {
		req.sendGetRequest(null);
	}
	@Ignore
	@Test
	public void potRequest() {
		req.sendPostRequest(null);
	}
}

