package org.aarna.test;

import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Ignore
import org.junit.Test

import org.aarna.modules.JsonParser
public class JsonParserTest {
	private static JsonParser   par;
	private static final String jsonStr = '''
{
    "correlationUUID": null,
    "commonHeader": {
        "timestamp": "2021-07-15T08:09:48.057Z",
        "originatorId": "SDNC_DG",
        "requestId": "e5eb1f1e-3386-435d-b290-d49d8af8db4c",
        "subRequestId": "143748f9-3cd5-4910-81c9-a4601ff2ea58",
        "flags": null
    },
    "actionIdentifiers": {
        "blueprintName": "multi-steps-py-1",
        "blueprintVersion": "1.0.0",
        "actionName": "remote-python",
        "mode": "sync"
    },
    "status": {
        "code": 200,
        "eventType": "EVENT_COMPONENT_EXECUTED",
        "timestamp": "2021-07-15T08:09:48.652Z",
        "errorMessage": null,
        "message": "success"
    },
    "payload": {
        "remote-python-response": {
            "status": "success",
            "response-data": {
                "EchoTest1-request": {
                    "project-id": "25313055"
                }
            }
        }
    }
}
'''
	@BeforeClass
	public static void init() {
		par = new JsonParser(jsonStr);
	}
	@Before
	public void beforeEachTest() {
		System.out.println("Starting next test case...");
	}
	@After
	public void afterEachTest() {
		System.out.println("Done with the test case...");
	}
	@Ignore
	@Test
	public void testIt() {
		println("Calling testIt");
		par.start();
	}
	@Test
	public void testStatus() {
		println("Calling getStatus");
		par.getStatus();
	}
	@Test
	public void testStatusCode() {
		println("Calling getStatusCode");
		assert (par.getStatusCode()) == 200
	}
	@Test
	public void testsetStatusFlag() {
		println("Calling setStatusFlag");
		assert (par.setStatusFlag()) == true
	}
}

